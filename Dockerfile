FROM pytorch/pytorch:1.13.0-cuda11.6-cudnn8-runtime

RUN git lfs install

ENV GIT_LFS_SKIP_SMUDGE=1

WORKDIR /bloom

ENTRYPOINT ["entrypoint.sh"]